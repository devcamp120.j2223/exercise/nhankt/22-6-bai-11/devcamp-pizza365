import { Component } from "react";

class Footer extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid bg-warning p-5">
                    <div className="row text-center">
                        <div className="col-sm-12">
                            <h4 className="m-2">Footer</h4>
                            <a href="#" className="btn btn-dark m-3"><i className="fa fa-arrow-up"></i>To the top</a>
                            <div className="m-2">
                                <i className="fa fa-facebook-official w3-hover-opacity"></i>
                                <i className="fa fa-instagram w3-hover-opacity"></i>
                                <i className="fa fa-snapchat w3-hover-opacity"></i>
                                <i className="fa fa-pinterest-p w3-hover-opacity"></i>
                                <i className="fa fa-twitter w3-hover-opacity"></i>
                                <i className="fa fa-linkedin w3-hover-opacity"></i>
                            </div>
                            Powered by Devcamp
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Footer