import { Component } from "react";

class Header extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid bg-light">
                    <div className="row">
                        <div className="col-sm-12">
                            <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
                                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="navbar-toggler-icon"></span>
                                </button>
                                <div className="collapse navbar-collapse bg-warning" id="navbarNav">
                                    <ul className="navbar-nav nav-fill w-100">
                                        <li className="nav-item active">
                                            <a className="nav-link" href="#">Trang chủ</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#plans">Combo</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#about">Loại Pizza</a>
                                        </li>
                                        <li className="nav-item" href="#">
                                            <a href="#contact" className="nav-link">Gửi đơn hàng</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Header