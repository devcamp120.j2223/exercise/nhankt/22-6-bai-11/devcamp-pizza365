import { Component } from "react";

class SizeComponent extends Component {
    render() {
        return (
            <div>
                <div id="plans" className="row">

<div className="col-sm-12 text-center p-4 mt-4">
  <h2><b className="p-1 border-bottom" style={{ fontFamily: "Cursive", color: "rgba(189, 186, 4, 0.726)" }}>Chọn size
    Pizza</b></h2>
  <p><span className="p-2 text-warning"><b>Hãy chọn cỡ pizza phù hợp với bạn!</b></span></p>
</div>

<div className="col-sm-12">
  <div className="row">
    <div className="col-sm-4">
      <div className="card">
        <div className="card-header bg-warning text-white text-center" style={{ height: "300px" }}>
          <h3>S (small)</h3>
          <br />
          <br />
          <img src={require("../../assets/images/s.jpg")} style={{ height: "150px" }} />
        </div>
        <div className="card-body text-center">
          <ul className="list-group list-group-flush">
            <li className="list-group-item"> Đường kính: <b>20cm</b></li>
            <li className="list-group-item"> Sườn nướng: <b>2</b></li>
            <li className="list-group-item"> Salad: <b>200g</b></li>
            <li className="list-group-item"> Nước ngọt: <b>2</b></li>
            <li className="list-group-item">
              <h3><b>150.000 <br />VND</b></h3>
            </li>
          </ul>
        </div>
        <div className="card-footer  text-center">
          <button id="btn-small" 
            className="btn form-control btn-warning">Chọn</button>
        </div>
      </div>
    </div>
    <div className="col-sm-4">
      <div className="card">
        <div className="card-header bg-warning text-white text-center" style={{ height: "300px" }}>
          <h3>M (Medium)</h3>
          <br />
          <img src={require("../../assets/images/m.jpg")} style={{ height: "200px" }} />
        </div>
        <div className="card-body text-center">
          <ul className="list-group list-group-flush">
            <li className="list-group-item"> Đường kính: <b>25cm</b></li>
            <li className="list-group-item"> Sườn nướng: <b>4</b></li>
            <li className="list-group-item"> Salad: <b>300g</b></li>
            <li className="list-group-item"> Nước ngọt: <b>3</b></li>
            <li className="list-group-item">
              <h3><b>200.000 <br />VND</b></h3>
            </li>
          </ul>
        </div>
        <div className="card-footer text-center">
          <button id="btn-medium"
            className="btn form-control btn-warning">Chọn</button>
        </div>
      </div>
    </div>
    <div className="col-sm-4">
      <div className="card">
        <div className="card-header bg-warning text-white text-center" style={{ height: "300px" }}>
          <h3>L (Large)</h3>
          <img src={require("../../assets/images/L.jpg")} style={{ height: "240px" }} />
        </div>
        <div className="card-body text-center">
          <ul className="list-group list-group-flush">
            <li className="list-group-item"> Đường kính: <b>30cm</b></li>
            <li className="list-group-item"> Sườn nướng: <b>8</b></li>
            <li className="list-group-item"> Salad: <b>500g</b></li>
            <li className="list-group-item"> Nước ngọt: <b>4</b></li>
            <li className="list-group-item">
              <h3><b>250.000 <br />VND</b></h3>
            </li>
          </ul>
        </div>
        <div className="card-footer text-center">
          <button id="btn-large" 
            className="btn form-control btn-warning">Chọn</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
            </div>
        )
    }
}
export default SizeComponent