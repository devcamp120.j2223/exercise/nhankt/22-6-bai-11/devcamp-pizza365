import { Component } from "react";
import { Carousel } from 'react-bootstrap';

class IntroduceComponent extends Component {
    render() {
        return (
            <div>
                <div className="row">
              <div className="col-sm-12">
                <b className="text-warning">
                  <h1>Pizza 365 <br /></h1>
                  <h3 style={{ fontStyle: "italic" }}> Truly italian !</h3>
                </b>
              </div>

              <Carousel>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={require("../../assets/images/1.jpg")}
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={require("../../assets/images/2.jpg")}
                     alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={require("../../assets/images/3.jpg")} 
                    alt="Third slide" 
                  />
                </Carousel.Item>
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={require("../../assets/images/4.jpg")}
                     alt="Fourth slide"
                  />
                </Carousel.Item>
              </Carousel>
              <div className="col-sm-12 text-center p-4 mt-4" style={{ fontFamily: "Cursive", color: "rgba(8, 104, 59, 0.726)" }}>
                <h2><b className="p-2 border-bottom text-warning">Tại sao lại là 365 Pizza</b></h2>
              </div>


              <div className="col-sm-12">
                <div className="row">
                  <div className="col-sm-3 p-4 border" style={{ backgroundColor: "lightgoldenrodyellow" }}>
                    <h3 className="p-2" style={{ fontFamily: "Monospace" }}>Vị trí</h3>
                    <p className="p-2" style={{ fontFamily: "Monospace" }}>Tọa lạc gần trung tâm thành phố, 1 nơi lý tưởng để mọi
                      người gặp nhau sau giờ làm việc căng thẳng, thưởng thức đầy đủ sự đa dạng của thế giới pizza</p>
                  </div>
                  <div className="col-sm-3 p-4  border" style={{ backgroundColor: "yellow" }}>
                    <h3 className="p-2" style={{ fontFamily: "Monospace" }}>Nguyên liệu</h3>
                    <p className="p-2" style={{ fontFamily: "Monospace" }}>Đảm bảo 100% nguyên liêu tươi sống, rõ nguồn gốc và bảo
                      đảm vệ sinh an toàn thực phẩm</p>
                  </div>
                  <div className="col-sm-3 p-4  border" style={{ backgroundColor: "lightsalmon" }}>
                    <h3 className="p-2" style={{ fontFamily: "Monospace" }}>Hương vị</h3>
                    <p className="p-2" style={{ fontFamily: "Monospace" }}>Hương vị đặc trưng đến từ Italia pha trộn một chút nét Á
                      Đông khiến bạn không thể cưỡng lại</p>
                  </div>
                  <div className="col-sm-3 p-4   border" style={{ backgroundColor: "orange" }}>
                    <h3 className="p-2" style={{ fontFamily: "Monospace" }}>Dịch vụ</h3>
                    <p className="p-2" style={{ fontFamily: "Monospace" }}>Nhân viên thân thiện, nhà hàng hiện đại với đầy đủ các
                      tiện ích, giao hàng nhanh cho đơn online.Giảm giá vào thứ 5 hàng tuần và luôn có những voucher hấp dẫn
                      cho thành viên</p>
                  </div>
                </div>
              </div>
            </div>
            </div>
        )
    }
}
export default IntroduceComponent