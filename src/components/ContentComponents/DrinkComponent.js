import { Component } from "react";

class DrinkComponent extends Component {
    render() {
        return (
            <div>
                <div className="text-center mt-4">
              <h3><label className="text-warning border-bottom border-warning">Chọn đồ Uống</label></h3>
              <select id="select-drink" style={{ width: "400px" }}>
                <option value="0">Chọn 1 loại đồ uống </option>
              </select>
            </div>
            </div>
        )
    }
}
export default DrinkComponent