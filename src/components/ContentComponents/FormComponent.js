import { Component } from "react";

class FormComponent extends Component {
    render() {
        return (
            <div>
                <div id="contact" className="row">

<div className="col-sm-12 text-center p-4 mt-4">
  <h2><b className="p-2 border-bottom border-warning text-warning" style={{ fontFamily: "Cursive" }}>Gửi đơn hàng</b>
  </h2>
</div>


<div className="col-sm-12 p-2 jumbotron">
  <div className="row">
    <div className="col-sm-12">
      <div className="form-group">
        <label htmlFor="inp-fullname">Họ và tên</label>
        <input type="text" className="form-control" id="inp-fullname" placeholder="Họ và tên" />
      </div>
      <div className="form-group">
        <label htmlFor="inp-email">Email</label>
        <input type="text" className="form-control" id="inp-email" placeholder="Email" />
      </div>
      <div className="form-group">
        <label htmlFor="inp-phone">Điện thoại</label>
        <input type="text" className="form-control" id="inp-phone" placeholder="Điện thoại" />
      </div>
      <div className="form-group">
        <label htmlFor="inp-address">Địa chỉ</label>
        <input type="text" className="form-control" id="inp-address" placeholder="Địa chỉ" />
      </div>
      <div className="form-group">
        <label htmlFor="inp-voucherID">Mã giảm giá (voucherID)</label>
        <input type="text" className="form-control" id="inp-voucherID" placeholder="Mã voucher" />
      </div>
      <div className="form-group">
        <label htmlFor="inp-message">Lời nhắn</label>
        <input type="text" className="form-control" id="inp-message" placeholder="Lời nhắn" />
      </div>
      <button type="button"  className="btn btn-warning w-100">Gửi</button>
    </div>
  </div>
</div>

<div id="div-order-confirm" className="container bg-warning p-2 jumbotron" style={{ display: "none" }}>
  <div className="row">
    <div className="col-sm-12">Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là:</div>
  </div>
  <div className="row mt-4">
    <div className="col-sm-4">Mã đơn hàng:</div>
    <div id="orderid" className="col-sm-7 border border-dark">..</div>
  </div>
</div>
</div>
            </div>
        )
    }
}
export default FormComponent