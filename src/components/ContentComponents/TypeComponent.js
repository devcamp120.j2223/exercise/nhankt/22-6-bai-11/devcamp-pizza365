import { Component } from "react";

class TypeComponent extends Component {
    render() {
        return (
            <div>
                <div id="about" className="row">

<div className="col-sm-12 text-center p-4 mt-4">
  <h2><b className="p-2 border-bottom text-warning" style={{ fontFamily: "Cursive" }} >Chọn loại Pizza</b></h2>
</div>


<div className="col-sm-12">
  <div className="row">
    <div className="col-sm-4">
      <div className="card w-100" style={{ width: "18rem" }}>
        <img src={require("../../assets/images/hawaiian.jpg")} className="card-img-top" />
        <div className="card-body" style={{ fontFamily: "Monospace" }}>
          <h3>Hawai</h3>
          <p style={{ height: "150px" }}>Vị thanh của dứa, vị đậm đà của thịt xông khói kết hợp với Đế bánh dai,
            giòn tạo nên hương vị phóng khoáng của Pizza Hawaii.</p>
          <p><button id="btn-type1" 
            className="btn btn-warning w-100">Chọn</button></p>
        </div>
      </div>
    </div>
    <div className="col-sm-4">
      <div className="card w-100" style={{ width: "18rem" }}>
        <img src={require("../../assets/images/seafood.jpg")} className="card-img-top" />
        <div className="card-body" style={{ fontFamily: "Monospace" }}>
          <h3>Hải sản</h3>
          <p style={{ height: "150px" }}>Món ăn độc đáo và sang trọng, sở hữu vị thơm ,béo của phô mai, vị ngọt của
            hải sản, vị chua nhè nhẹ của sốt cà.</p>
          <p><button id="btn-type2" 
            className="btn btn-warning w-100">Chọn</button></p>
        </div>
      </div>
    </div>
    <div className="col-sm-4">
      <div className="card w-100" style={{ width: "18rem" }}>
        <img src={require("../../assets/images/bacon.jpg")} className="card-img-top" />
        <div className="card-body" style={{ fontFamily: "Monospace" }}>
          <h3>Thịt xông khói</h3>
          <p style={{ height: "150px" }}>Pizza kết hợp giữa thịt giăm bông, thịt xông khói và hai loại ớt xanh, cà
            chua. Sự pha trộn vị giác giữa các loại thịt sẽ lôi cuốn bạn.</p>
          <p><button id="btn-type3" 
            className="btn btn-warning w-100">Chọn</button></p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
            </div>
        )
    }
}
export default TypeComponent