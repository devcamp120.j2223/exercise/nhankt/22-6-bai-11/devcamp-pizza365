import { Component } from "react";
import DrinkComponent from "./ContentComponents/DrinkComponent";
import FormComponent from "./ContentComponents/FormComponent";
import IntroduceComponent from "./ContentComponents/IntroduceComponent";
import SizeComponent from "./ContentComponents/SizeComponent";
import TypeComponent from "./ContentComponents/TypeComponent";


class Content extends Component {
    render() {
        return (
            <div>
                <div className="container" color="rgb(12, 14, 8)" style={{ padding: "60px 0 50px 0" }}>
                    <div className="row">
                        <div className="col-sm-12">
                        <IntroduceComponent></IntroduceComponent>
                        <SizeComponent></SizeComponent>
                        <TypeComponent></TypeComponent>
                        <DrinkComponent></DrinkComponent>
                        <FormComponent></FormComponent>
                    </div>
                </div>
            </div>
            </div >
        )
    }
}
export default Content